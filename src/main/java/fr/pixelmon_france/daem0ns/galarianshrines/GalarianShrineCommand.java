package fr.pixelmon_france.daem0ns.galarianshrines;

import java.util.Collections;
import java.util.List;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class GalarianShrineCommand extends CommandBase {

	public String getName() {
		return "galarianshrine";
	}

	@Override
	public List<String> getAliases() {
		return Collections.emptyList();
	}

	@Override
	public String getUsage(ICommandSender icommandsender) {
		return "/galarianshrine reload";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		if (args.length == 0) {
			sender.sendMessage(new TextComponentString("==============" + TextFormatting.RED + "GalarianShrine"
					+ TextFormatting.RESET + "=============="));
			sender.sendMessage(new TextComponentString("      " + TextFormatting.AQUA + "This plugin was made by "
					+ TextFormatting.RED + "DaeM0nS      "));
			sender.sendMessage(new TextComponentString(
					TextFormatting.AQUA + "This plugin add a chance to summon a galarian bird from a shrine."));
			sender.sendMessage(new TextComponentString("==============" + TextFormatting.RED + "GalarianShrine"
					+ TextFormatting.RESET + "=============="));
			return;
		} else if (args.length == 1 && args[0].equalsIgnoreCase("reload")) {
			Config.instance.readConfig();
			sender.sendMessage(new TextComponentString(TextFormatting.RED + "[GalarianShrine] You just reload the Galarian Shrine config."));
			return;
		} else {
			sender.sendMessage(new TextComponentString(TextFormatting.RED + "[GalarianShrine] Wrong usage."));
			sender.sendMessage(new TextComponentString(getUsage(sender)));
			return;
		}
	}

	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
		if (args.length == 1) {
			return CommandBase.getListOfStringsMatchingLastWord(args, "reload");
		}
		return Collections.emptyList();
	}
}