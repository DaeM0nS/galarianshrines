package fr.pixelmon_france.daem0ns.galarianshrines;

import com.pixelmonmod.pixelmon.api.events.PlayerActivateShrineEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.apache.logging.log4j.Logger;

import com.pixelmonmod.pixelmon.Pixelmon;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

@Mod(modid="galarianshrines", name="GalarianShrines", version="1.0.0",  acceptableRemoteVersions="*", acceptedMinecraftVersions="[1.12.2]", dependencies = "required-after:pixelmon", serverSideOnly = true)
public class GalarianShrine {
    private static Logger logger;
	@Instance("galarianshrines")
    private static GalarianShrine instance;
    	
    @Mod.EventHandler
    public void onPreInit(FMLPreInitializationEvent event) {
    	logger = event.getModLog();
		if(Config.instance==null) new Config();
		Config.instance.readConfig();
		
		MinecraftForge.EVENT_BUS.register(this);
		Pixelmon.EVENT_BUS.register(this);
    }

    @Mod.EventHandler
    public void onServerStart(FMLServerStartingEvent event) {
    	event.registerServerCommand(new GalarianShrineCommand());
    }

    public static Logger getLogger(){ return logger; }
}
