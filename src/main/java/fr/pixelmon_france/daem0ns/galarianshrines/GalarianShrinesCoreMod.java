package fr.pixelmon_france.daem0ns.galarianshrines;

import java.io.File;
import java.io.FileInputStream;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;
import org.spongepowered.asm.launch.MixinBootstrap;
import org.spongepowered.asm.mixin.Mixins;

import net.minecraft.launchwrapper.Launch;
import net.minecraftforge.fml.relauncher.CoreModManager;
import net.minecraftforge.fml.relauncher.IFMLLoadingPlugin;

public class GalarianShrinesCoreMod implements IFMLLoadingPlugin {

    public GalarianShrinesCoreMod() {
        MixinBootstrap.init();
        Mixins.addConfiguration("mixins.galarianshrines.json");
    }

    @Override
    public String[] getASMTransformerClass() {
        return new String[0];
    }

    @Override
    public String getModContainerClass() {
        return null;
    }

    @Override
    public String getSetupClass() {
        return null;
    }

    @Override
    public void injectData(Map<String, Object> data) {
    	try {
    		ModsScanner.findAndLoad("com/pixelmonmod/pixelmon/Pixelmon.class");
    	} catch (Exception e) {
    		 System.out.println("There was a problem trying to load the pixelmon jar. The program will not continue.");
    		 e.printStackTrace();
    	}
    }
    
    @Override
    public String getAccessTransformerClass() {
        return null;
    }
}	 
class ModsScanner {
	/**
	 * @param clazz A class name that we're sure it's unique to our mod, the main mod class is perfect.
	 * @throws Exception Catch any possible exception, there could be a lot.
	 */
	public static void findAndLoad(String clazz) throws Exception {
		if (clazz == null || !clazz.endsWith(".class")) {
			throw new RuntimeException("The class is null or invalid. Class: " + clazz);
		}

		try {
			Class.forName(clazz, false, ModsScanner.class.getClassLoader());

			// The class is already there, we don't need to load it.
			return;
		} catch (Exception ignored) {}

		File mod;

		String provided = System.getProperty(clazz.substring(0, clazz.indexOf(".class")).replace("/", "."));
		if (provided != null) {
			mod = new File(provided);
			if (!mod.exists() || !containsClass(clazz, mod)) {
				throw new RuntimeException("The provided library didn't exist or did not contain the specified class. Path: " + mod.getAbsolutePath());
			}
		} else {
			File modsFolder = new File(System.getProperty("user.dir"), "mods");
			if (!modsFolder.exists()) {
				throw new RuntimeException("The mods folder couldn't be found. Path: " + modsFolder.toString());
			}

			// Scanning through /mods/ searching for all the jars
			mod = FileUtils.listFiles(modsFolder, new String[]{"jar"}, false).stream()
					.filter(file -> containsClass(clazz, file))
					.findAny().orElse(null);

			// Opsie dopsie, no jar found :(
			if (mod == null) {
				throw new RuntimeException("The mod's jar cannot be found.");
			}
		}

		// Once we found the mod, it's better to check if another coremod already loaded it
		if (!CoreModManager.getReparseableCoremods().contains(mod.getName())) {

			// Now we're safe to load the jar (thanks to clienthax)
			System.out.println("Loading the following mod for Galarian Shrines = " + mod.toURI().toURL().toString());
            Launch.classLoader.addURL(mod.toURI().toURL());
            CoreModManager.getReparseableCoremods().add(mod.getName());
		}
	}

	/**
	 * @param clazz The class to search.
	 * @param file The file to analyze.
	 * @return If the given file contains the given class.
	 */
	private static boolean containsClass(String clazz, File file) {
		try (ZipInputStream zip = new ZipInputStream(new FileInputStream(file))) {
			ZipEntry entry;
			while ((entry = zip.getNextEntry()) != null) {
				zip.closeEntry();

				if (entry.getName().equals(clazz)) {
					return true;
				}
			}
		} catch (Exception ignored) {}

		return false;
	}
}
