package fr.pixelmon_france.daem0ns.galarianshrines.mixin;

import java.util.Random;

import com.pixelmonmod.pixelmon.enums.forms.RegionalForms;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;

import com.pixelmonmod.pixelmon.blocks.tileEntities.TileEntityShrine;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;

import fr.pixelmon_france.daem0ns.galarianshrines.Config;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(TileEntityShrine.class)
public class MixinTileEntityShrine {

    /**
     * @author DaeM0nS
     */
    @Redirect(method = "activate", at = @At(
            value = "INVOKE",
            target = "Lcom/pixelmonmod/pixelmon/entities/pixelmon/EntityPixelmon;func_70107_b(DDD)V",
            opcode = Opcodes.INVOKESTATIC,
            remap = false
    ), remap = false)
    public void galarianShrine(EntityPixelmon instance, double x, double y, double z){
        instance.setPosition(x, y, z);
        if(new Random().nextInt(100)<=Config.chance){
            instance.setForm(RegionalForms.GALARIAN);
            instance.updateTransformed();
        }
    }
}
