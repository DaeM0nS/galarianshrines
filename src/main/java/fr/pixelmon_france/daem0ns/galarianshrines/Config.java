package fr.pixelmon_france.daem0ns.galarianshrines;

import java.io.File;

import org.apache.logging.log4j.Level;

import net.minecraftforge.common.config.Configuration;

public class Config {
	
	public static Config instance;
	
	public static Configuration cfg;
	
    public static final String CATEGORY_GENERAL = "General";
    
    //timeout list
    public static int chance = 20;
    
    public Config() {
    	instance = this;
    	cfg = new Configuration(new File("./config/GalarianShrines.cfg"));
    }
    
    public void readConfig() {
        try {
            cfg.load();
            initGeneralConfig(cfg);
        } catch (Exception e1) {
            GalarianShrine.getLogger().log(Level.WARN, "Problem while loading galarian shrines config file!", e1);
        } finally {
            if (cfg.hasChanged()) {
                cfg.save();
            }
            GalarianShrine.getLogger().info("Galarian shrines configuration file loaded !");
        }        
    }

    private void initGeneralConfig(Configuration cfg) {
        cfg.addCustomCategoryComment(CATEGORY_GENERAL, "General Configuration");
        chance = cfg.getInt("chance", CATEGORY_GENERAL, chance, 0, 100, "Chance to have a galarian (if random from 0 -> 100 <= chance)");
    }
}